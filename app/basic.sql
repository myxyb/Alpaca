
DROP TABLE IF EXISTS `site`;
CREATE TABLE `site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` char(20) NOT NULL,
  `name` char(30) NOT NULL,
  `domain` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `node`;
CREATE TABLE IF NOT EXISTS `node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` char(20) NOT NULL,
  `upid` int(11) NOT NULL,
  `name` char(20) NOT NULL,
  `type` char(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `ext` text NOT NULL,
  `ext1` text NOT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '999',
  `create_date` int(11) NOT NULL,
  `update_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `node` (`id`, `upid`, `name`, `type`, `title`, `content`, `ext`, `ext1`, `tags`, `order`, `create_date`, `update_date`) VALUES
(1, 0, 'home', 'page', '网站首页', '<p>\r\n	本页面为示例页面，请点击 <a href="?/admin">?/admin</a>&nbsp;进入后台进行管理，初始帐号：\r\n</p>\r\n<ul>\r\n	<li>\r\n		<span style="font-size:12px;">用户名 admin</span> \r\n	</li>\r\n	<li>\r\n		<span style="font-size:12px;">密码 &nbsp;admin&nbsp;</span> \r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	欢迎访问羊驼官网 <a href="http://alpaca.b24.cn" target="_blank">http://alpaca.b24.cn</a> ,提出宝贵建议。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>', 'subtitle:一句话简介、格言', '{"meta_keywords":"","meta_description":"","page_title":"\\u7f8a\\u9a7c CMS","template":"","layout":""}', '首页', 1, 0, 1378979337),
(6, 0, 'links', 'page', '友情链接', '<ul class="square">\r\n	<li class="first">\r\n		<a href="http://eku.b24.cn/" target="_blank"><span>一库仓储</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://www.pepm.com.cn/" target="_blank"><span>投资管理系统</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://b2core.b24.cn/" target="_blank"><span>b2Core</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://raya.cn/"><span>百家争鸣</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://www.cainiaozhaomama.com/" target="_blank"><span>菜鸟找妈妈</span></a> \r\n	</li>\r\n	<li class="first">\r\n		<a href="http://tech20.b24.cn/" target="_blank"><span>TECH20</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://beiercn.com/" target="_blank"><span>贝尔塑料</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://www.allyesbuy.com/" target="_blank"><span>全是买代购</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://yu.b24.cn/" target="_blank"><span>水族之家</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://daweiart.tk/" target="_blank"><span>大伟艺网</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://yangchenglake.net/" target="_blank"><span>阳澄湖度假区</span></a> \r\n	</li>\r\n	<li>\r\n		<a href="http://iaqua.cn/" target="_blank"><span>易水族</span></a> \r\n	</li>\r\n</ul>', '', '{"meta_keywords":"","meta_description":"","page_title":"","template":"","layout":""}', '', 1001, 1378976339, 1378979279),
(2, 0, 'menu', 'link', '', '', '[{"title":"\\u9996\\u9875","name":"?\\/","order":"1"},{"title":"\\u5173\\u4e8e","name":"?\\/home\\/page\\/about","order":"2"}]', '', NULL, 999, 1378280228, 1378285263),
(3, 0, 'about', 'page', '关于', '<h2>\r\n</h2>\r\n<h1>\r\n	羊驼！CMS\r\n</h1>\r\n<h2>\r\n	羊驼！羊驼！前方惊现羊驼 !\r\n</h2>\r\n<p>\r\n	羊驼！ 是一个开源的轻量级树状 CMS 系统。 它基于 php + mysql 并以 b2core MVC 为底层架构。 可以方便快速的配置出个人、企业网站。在B2Core 优雅的MVC架构之上你可以轻松定制任意应用型网站。\r\n</p>\r\n<h2>\r\n	功能特性\r\n</h2>\r\n<ul>\r\n	<li>\r\n		方便上手， 5 分钟即可定制出简易的网站\r\n	</li>\r\n	<li>\r\n		支持 Mysql / SQlite 数据库\r\n	</li>\r\n	<li>\r\n		无限级别子栏目\r\n	</li>\r\n	<li>\r\n		子栏目可设置独立风格(站中站)\r\n	</li>\r\n	<li>\r\n		支持自定义原型属性，可以管理复杂的页面\r\n	</li>\r\n	<li>\r\n		可实现复杂页面排版\r\n	</li>\r\n	<li>\r\n		静态url, 面向 SEO 优化\r\n	</li>\r\n	<li>\r\n		数据实时备份、恢复\r\n	</li>\r\n	<li>\r\n		支持缓存\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>', 'subtitle:关于企业', '{"meta_keywords":"","meta_description":"","page_title":"\\u5173\\u4e8e\\u4f01\\u4e1a","template":"","layout":""}', '关于', 1000, 1378283471, 1378976716),
(4, 0, 'sidebar', 'link', '', '', '[{"title":"\\u4ea7\\u54c1&\\u670d\\u52a1","name":"?\\/home\\/page\\/product","order":"1"},{"title":"\\u53cb\\u60c5\\u94fe\\u63a5","name":"?\\/home\\/page\\/links","order":"2"}]', '', NULL, 999, 1378285463, 1378285463),
(5, 0, 'tags', 'tag', '', '', '[{"name":"sitename","val":"\\u7f51\\u7ad9\\u6807\\u9898"},{"name":"subsitename","val":"\\u7f51\\u7ad9\\u526f\\u6807\\u9898"}]', '', NULL, 999, 1378290171, 1378290171),
(7, 0, 'product', 'page', '网站建设服务', '<h1>\r\n	网站建设服务\r\n</h1>\r\n<p>\r\n	如果需要基于羊驼的网站建设服务可以跟我们联系 b400tk@163.com\r\n</p>\r\n<p>\r\n	价格 &nbsp;800 元起\r\n</p>\r\n<h2>\r\n	案例展示\r\n</h2>\r\n<ul class="square">\r\n	<li class="first">\r\n		<a href="http://tech20.b24.cn/" target="_blank"><span>TECH20</span></a>\r\n	</li>\r\n	<li>\r\n		<a href="http://beiercn.com/" target="_blank"><span>贝尔塑料</span></a>\r\n	</li>\r\n	<li>\r\n		<a href="http://www.allyesbuy.com/" target="_blank"><span>全是买代购</span></a>\r\n	</li>\r\n	<li>\r\n		<a href="http://yu.b24.cn/" target="_blank"><span>水族之家</span></a>\r\n	</li>\r\n	<li>\r\n		<a href="http://daweiart.tk/" target="_blank"><span>大伟艺网</span></a>\r\n	</li>\r\n	<li>\r\n		<a href="http://yangchenglake.net/" target="_blank"><span>阳澄湖度假区</span></a>\r\n	</li>\r\n	<li>\r\n		<a href="http://iaqua.cn/" target="_blank"><span>易水族</span></a>\r\n	</li>\r\n</ul>', '', '{"meta_keywords":"","meta_description":"","page_title":"","template":"","layout":""}', '', 1002, 1378976689, 1378976952);

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text,
  `username` text,
  `password` text,
  `post_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `info` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `user` (`id`, `email`, `username`, `password`, `post_time`, `update_time`, `level`, `info`) VALUES
(1, 'admin@local.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1334938996, 1377847009, 20, NULL);
