<div class="module_baner pc_baner">
		<div class="module_baner_txt">
			<div class="module_baner_txt_title pc_baner_txt_title">
				<h2>核心功能模块</h2>
			</div>
			<div class="module_baner_txt_xian index_xian"></div>
			<div class="module_baner_txt_con pc_baner_txt_con">
				<h3>
					<p><?=$content?></p>
				</h3>
			</div>
		</div>
	</div>
	<div class="module_content">
		<?=alpa_list("module","module_con")?>
	</div>
	<style>
		.swiper-container {
		    width: 100%;
		    height: 100%;
		}

		.swiper-slide {
		    font-size: 18px;
		    height: auto;
		    -webkit-box-sizing: border-box;
		    box-sizing: border-box;
		    padding: 3% 30px;
		}
		.layui-tab-brief>.layui-tab-title .layui-this{
			color: #de0000;
		}
		.layui-tab-brief>.layui-tab-more li.layui-this:after, .layui-tab-brief>.layui-tab-title .layui-this:after{
			border-bottom: 2px solid #de0000;
		}
	</style>
	<script>
        var swiper = new Swiper('.swiper-container', {
            direction: 'vertical',
            slidesPerView: 'auto',
            freeMode: true,
            observer:true,
            observeParents:true,
            scrollbar: {
                el: '.swiper-scrollbar',
            },
            mousewheel: true,
        });
    </script>