<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?=$meta_keywords?$meta_keywords:$title?>" />
    <meta name="description" content="<?=$meta_description?$meta_description:$title?>" />
    <title><?=$page_title?$page_title:$title?> - PEPM</title>
    <link rel="stylesheet" href="/static/ceshi/css/pepm.css">
    <link rel="stylesheet" href="/static/ceshi/css/swiper.min.css">
    <script src="/static/ceshi/js/swiper.min.js"></script>
    <style>
    /*swiper*/
    .index_core_table_card {
      width: 100%;
      padding-top: 40px;
      padding-bottom: 44px;
      height: 345px;
    }
    .index_core_table_card_list > .swiper-slide {
      background-position: center;
      background-size: cover;
      width: 300px;
      height: 300px;
      box-shadow:0 0 10px 0 #000000;
    }
    .swiper-pagination{
    	width: 200px !important;
    	overflow:visible !important;
    	height: 40px;
    }
    .swiper-pagination p{
    	font-size: 33px !important;
    	color: #353535 !important;
    }
    .swiper-pagination-bullet{
    	background-color:transparent !important;
    }
    .swiper-pagination-bullet-active-main{
    	display: block !important;
    	left: 40px 	!important;
    }

    /*custmer*/
    .index_customer_table_card {
      width: 100%;
      height: 100%;
    }
    .index_customer_table_card_con {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
    .index_customer_table_card > .swiper-wrapper{
    	height: 220px;
    }
    .index_customer_table_card > .swiper-button-next{
    	background-image: url(images/right.png) !important;
    	margin-right: 15%;
    	width: 30px;
    }
    .index_customer_table_card > .swiper-button-prev{
    	background-image: url(images/left.png) !important;
    	margin-left: 15%;
    	width: 30px;
    }
  </style>
</head>
<body>
	<div class="m_head">
		<div class="m_logo">
			<a href="/"><img src="images/logo-1.png"></a>
		</div>
		<div class="nav_box_m">
			<div class="nav_m">
				<ul class="nav_list">
					<li><a href="index.html" class="nav_list_1 nav_list_on"><span>首页</span></a></li>
					<li><a href="module.html" class="nav_list_2"><span>功能模块</span></a></li>
					<li><a href="case.html" class="nav_list_3"><span>案例</span></a></li>
					<li><a href="about.html" class="nav_list_4"><span>关于我们</span></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="head gray_back">
		<div class="head_con">
			<div class="head_logo">
				<img src="images/logo-1.png">
			</div>
			<div class="nav_box">
				<div class="nav">
					<ul class="nav_list_p">
						<?=alpa('daohang','daohang')?>
						<!-- <li><a href="index.html" class="nav_list_p_1 nav_list_p_on"><span>首页</span></a></li>
						<li><a href="module.html" class="nav_list_p_2"><span>功能模块</span></a></li>
						<li><a href="case.html" class="nav_list_p_3"><span>案例</span></a></li>
						<li><a href="about.html" class="nav_list_p_4"><span>关于我们</span></a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- end_head -->
<?=$al_content?>
<!-- footer -->
	<div class="m_footer">
		<div class="m_footer_txt">
			<p><strong>中国领先的PE投资管理软件</strong></p>
		</div>
		<div class="m_footer_code">
			<div class="m_footer_code_imgbox">
				<a href=""><img src="images/img_4.png"></a>
			</div>
			<div class="m_footer_code_txt">
				<p><strong>长按二维码申请试用</strong></p>
			</div>
		</div>
		<div class="m_footer_ardess">
			<p><strong>江苏省苏州市中国苏州创业园4号楼</strong></p>
			<p><strong>0512-68075637</strong></p>
			<p><strong>www.pepm.com.cn</strong></p>
		</div>
	</div>
	<div class="footer">
		<div class="footer_con">
			<div class="footer_con_title">
				<p>中国领先的PE投资管理软件</p>
			</div>
			<div class="footer_link">
				<div class="footer_con_left">
					<div class="footer_con_left_img">
						<img src="images/img_4.png">
					</div>
					<div class="footer_con_left_con">
						<p>扫码申请试用</p>
					</div>
				</div>
				<div class="footer_con_right_al">
					<div class="footer_con_right footer_con_right_1">
						<div class="footer_con_right_title">
							<p>友情链接</p>
						</div>
						<div class="footer_con_right_xian index_xian"></div>
						<div class="footer_con_right_con">
							<ul>
								<li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>风潮 Current.VC</p>
										</div>
									</div>
								</li>
								<li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>IT经理世界</p>
										</div>
									</div>
								</li><li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>IT橘子</p>
										</div>
									</div>
								</li><li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>投中集团</p>
										</div>
									</div>
								</li><li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>VC800</p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="footer_con_right">
						<div class="footer_con_right_title">
							<p>友情链接</p>
						</div>
						<div class="footer_con_right_xian index_xian"></div>
						<div class="footer_con_right_con">
							<ul>
								<li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>风潮 Current.VC</p>
										</div>
									</div>
								</li>
								<li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>IT经理世界</p>
										</div>
									</div>
								</li><li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>IT橘子</p>
										</div>
									</div>
								</li><li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>投中集团</p>
										</div>
									</div>
								</li><li>
									<div class="footer_con_right_con_link">
										<div class="footer_con_right_con_icon">
											<img src="images/icon_48.png">
										</div>
										<div class="footer_con_right_con_word">
											<p>VC800</p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="footer_xian index_xian"></div>
		<div class="footer_contact">
			<p>江苏省苏州市中国苏州创业园4号楼</p>
			<p>0512-68075637</p>
			<p>www.pepm.com.cn</p>
		</div>
	</div>
	<script>
    var swiper = new Swiper('.index_core_table_card', {
	    effect: 'coverflow',
	    grabCursor: true,
	    centeredSlides: true,
	    slidesPerView: 'auto',
	    coverflowEffect: {
	      rotate: 50,
	      stretch: 0,
	      depth: 100,
	      modifier: 1,
	      slideShadows : true,
	    },
	    pagination: {
		    el: '.swiper-pagination',
		    clickable: true,
		    dynamicBullets: true,
    		dynamicMainBullets: 1,
		  renderBullet: function (index, className) {
		      switch(index){
		        case 0:text='项目池1';break;
		        case 1:text='项目池2';break;
		        case 2:text='项目池3';break;
		        case 3:text='项目池4';break;
		        case 4:text='项目池5';break;
		      }
		      return '<div class="' + className + '" style="display:none" ><p>'+text+'</p></div>';
		    },
		},
    });
    var swiper = new Swiper('.index_customer_table_card', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
</body>
</html>