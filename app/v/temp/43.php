<div class="about_banner  pc_baner">
		<div class="about_banner_txt">
			<div class="about_banner_txt_tilte  pc_baner_txt_title">
				<h2>公司介绍</h2>
			</div>
			<div class="about_banner_txt_xian index_xian"></div>
			<div class="about_banner_txt_content  pc_baner_txt_con">
				<h3>
					<?=$content?>
				</h3>
			</div>
		</div>
	</div>
	<div class="about_contact">
		<div class="about_contact_txt">
			<div class="about_contact_txt_title">
				<h2>联系我们</h2>
			</div>
			<div class="about_contact_txt_xian index_xian"></div>
			<div class="about_contact_txt_con layui-row">
				<div class="about_map layui-col-md6 layui-col-sm12 layui-col-xs12">
					<iframe name="toppage" width=100% height=100% marginwidth=0 marginheight=0 frameborder="no" border="0"  src="/map.html" ></iframe> 
				</div>	
				<div class="about_con layui-col-md6 layui-col-sm12 layui-col-xs12">
					<div class="about_contact_txt_con_list about_list_1 layui-row">
						<div class="about_contact_txt_con_list_img layui-col-md3 layui-col-sm4 layui-col-xs4">
							<img src="/static/ceshi/images/img_6.png">
						</div>
						<div class="about_contact_txt_con_list_title layui-col-md9 layui-col-sm8 layui-col-xs8">
							<p>电话：</br>0512-68075637</p>
						</div>
					</div>
					<div class="about_contact_txt_con_list about_list_2  layui-row">
						<div class="about_contact_txt_con_list_img layui-col-md3  layui-col-sm4 layui-col-xs4">
							<img src="/static/ceshi/images/img_7.png">
						</div>
						<div class="about_contact_txt_con_list_title layui-col-md9 layui-col-sm8 layui-col-xs8">
							<p>网址：</br>www.pepm.com.cn</p>
						</div>
					</div>
					<div class="about_contact_txt_con_list about_list_3 layui-row">
						<div class="about_contact_txt_con_list_img layui-col-md3 layui-col-sm4 layui-col-xs4">
							<img src="/static/ceshi/images/img_8.png">
						</div>
						<div class="about_contact_txt_con_list_title layui-col-md9 layui-col-sm8 layui-col-xs8">
							<p>地址：</br>江苏省苏州市高新区中国苏州创业园4号楼1409室</p>
						</div>
					</div>
					<div class="about_contact_txt_con_list about_list_4 layui-row">
						<div class="about_contact_txt_con_list_img layui-col-md3 layui-col-sm4 layui-col-xs4">
							<img src="/static/ceshi/images/img_9.png">
						</div>
						<div class="about_contact_txt_con_list_title layui-col-md9 layui-col-sm8 layui-col-xs8">
							<p>联系：</br>info@pepm.com.cn</br>技术支持：</br>support@pepm.com.cn</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>