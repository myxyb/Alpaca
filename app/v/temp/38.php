<div class="banner">
		<div class="banner_txt">
			<p>专业 · 高效 · 易用 · 创新</p>

		</div>
		<div class=" baner_more">
			<a href="https://alpha.vc800.com/?/open/id/foqe68vtcc" target="_blank" >申请试用</a>
		</div>
	</div>
	<div class="index_core">
		<div class="index_core_txt">
			<div class="index_core_title">
				<h2>FOF版/引导基金版</h2>
				<h2>核心功能模块</h2>
			</div>
			<div class="index_xian index_xian_core"></div>
			<div class="index_core_con">
				<h3>
					<p>让系统真正成为投资人</p>
					<p>移动办公和打单的利器</p>
				</h3>
			</div>
		</div>
		<div class="index_core_table">
			<div class="swiper-container index_core_table_card">
			    <div class="swiper-wrapper index_core_table_card_list">
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/a_3.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/b_2.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/c_2.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/d_1.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/e_2.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/f_4.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/g_1.jpg)"></div>
			      <div class="swiper-slide" style="background-image:url(/static/ceshi/images/module/h_1.jpg)"></div>
			    </div>
			    <div class="swiper-pagination core_dian"></div>
			</div>
		</div>
		<div class="index_core_more index_more ">
			<a href="/module" class="layui-btn">查看详情</a>
		</div>
	</div>
	<div class="gray_back">
		<div class="index_customer">
		<div class="index_customer_txt">
			<div class="index_customer_txt_title">
				<h2>部分客户</h2>
			</div>
			<div class="index_customer_txt_xian index_xian"></div>
			<div class="index_customer_txt_con">
				<h3>
					<p>我们正在见证以支撑用户高效完</p>
					<p>成工作为目的全新一代企业应用的诞生</p>
				</h3>
			</div>
		</div>
		
		<div class="customer_con">
			<div class="swiper-container index_customer_card">
		    <div class="swiper-wrapper">
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/1.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/2.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/3.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/4.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/5.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/6.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/7.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/8.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/9.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/10.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/11.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/12.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/13.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/14.jpg">
		      	</div>
		      </div>
		      <div class="swiper-slide layui-col-md3 layui-col-sm3 customer_box">
		      	<div class="customer_img_box">
		      		<img src="/static/ceshi/images/customer/15.jpg">
		      	</div>
		      </div>
		    </div>
		    <!-- Add Pagination -->
		    <div class="swiper-pagination customer_dian"></div>
		  </div>
		</div>
	</div>
	</div>
	
	<div class="index_case">
		<div class="index_case_txt">
			<div class="index_case_txt_title">
				<h2>案例</h2>
			</div>
			<div class="index_case_txt_xian index_xian"></div>
			<div class="index_case_txt_con">
				<h3>
					<p>让系统真正成为投资人</p>
					<p>移动办公和打单的利器</p>
				</h3>
			</div>
		</div>
		<div class="index_case_list layui-row">
			<div class="index_case_list_content layui-col-md6 layui-col-sm6">
				<div class="index_case_list_imgbox">
					<img src="/static/ceshi/images/index_case_1.jpg">
				</div>
				<div class="index_case_list_txt">
					<p>元禾辰坤创立于2006年，是中国第一支市场化专业母基金管理团队，目前管理四期母基金总资产管理规模超过200亿元人民币。</p>
				</div>
			</div>
			<div class="index_case_list_content layui-col-md6 layui-col-sm6">
				<div class="index_case_list_imgbox">
					<img src="/static/ceshi/images/index_case_2.jpg">
				</div>
				<div class="index_case_list_txt">
					<p>紫荆资本于2012年在原清华控股投资部的基础上更名并组建，是以清华控股母基金为主的紫荆系创业投资母基金投资、管理和运营平台，拥有丰富的母基金投资管理经验及行业资源。</p>
				</div>
			</div>
		</div>
		<div class="index_case_more index_more">
			<a href="/case" class="layui-btn">查看更多</a>
		</div>
		</div>
	<script>
    var swiper = new Swiper('.index_customer_card', {
      slidesPerView: 4,
      spaceBetween: 30,
      // init: false,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
  </script>