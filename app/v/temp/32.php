<div class="about_banner  pc_baner">
		<div class="about_banner_txt">
			<div class="about_banner_txt_tilte  pc_baner_txt_title">
				<h2>公司介绍</h2>
			</div>
			<div class="about_banner_txt_xian index_xian"></div>
			<div class="about_banner_txt_content  pc_baner_txt_con">
				<h3>
					<p>
						苏州梓川信息科技有限公司是一家创新型互联网企业软件服务商。公司致力于将成熟互联网技术（社交网络，移动，云）同企业业务应用结合，为企业用户提供更好用，专业且低成本的软件服务。
					</p>
					<p>
						公司创始团队成员来自软银投资、元禾控股、方正科技、绿盟等公司，具有多年管理软件，互联网和投资管理领域经验。多年来，我们深刻地体会到企业在购买昂贵、僵化且复杂难用的传统管理软件后，却因为用户不愿意使用等原因而带不来预期价值的普遍情况，传统投资管理软件居高不下的失败率便是这种状况的典型体现。
					</p>
				</h3>
			</div>
		</div>
	</div>
	<div class="about_contact">
		<div class="about_contact_txt">
			<div class="about_contact_txt_title">
				<h2>联系我们</h2>
			</div>
			<div class="about_contact_txt_xian index_xian"></div>
			<div class="about_contact_txt_con">
				<ul>
					<li>
						<div class="about_contact_txt_con_list about_list_1">
							<div class="about_contact_txt_con_list_img">
								<img src="images/img_6.png">
							</div>
							<div class="about_contact_txt_con_list_title">
								<p>电话</p>
							</div>
							<div class="about_contact_txt_con_list_txt list_table">
								<p>0512-68075637</p>
							</div>
						</div>
					</li>
					<li>
						<div class="about_contact_txt_con_list about_list_2">
							<div class="about_contact_txt_con_list_img">
								<img src="images/img_7.png">
							</div>
							<div class="about_contact_txt_con_list_title">
								<p>网址</p>
							</div>
							<div class="about_contact_txt_con_list_txt list_table">
								<p>www.pepm.com.cn</p>
							</div>
						</div>
					</li>
					<li>
						<div class="about_contact_txt_con_list about_list_3">
							<div class="about_contact_txt_con_list_img">
								<img src="images/img_8.png">
							</div>
							<div class="about_contact_txt_con_list_title">
								<p>地址</p>
							</div>
							<div class="about_contact_txt_con_list_txt">
								<p>江苏省苏州市高新区中国苏州创业园4号楼1409室</p>
							</div>
						</div>
					</li>
					<li>
						<div class="about_contact_txt_con_list about_list_4">
							<div class="about_contact_txt_con_list_img">
								<img src="images/img_9.png">
							</div>
							<div class="about_contact_txt_con_list_title">
								<p>邮件</p>
							</div>
							<div class="about_contact_txt_con_list_txt">
								<p>联系：info@pepm.com.cn</p>
								<p>技术支持：support@pepm.com.cn</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>