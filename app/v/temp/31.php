<div class="banner">
		<div class="banner_txt">
			<p>专业 · 高效 · 易用 · 创新</p>
		</div>
	</div>
	<div class="index_fof">
		<div class="index_fof_con gray_back">
			<div class="index_fof_title">
				<div class="index_fof_title_da">
					<h2>FOF版/引导基金版</h2>
				</div>
				<div class="index_xian"></div>
				<div class="index_fof_title_con">
					<h3>
						<p>我们研究了国内多家知名母基金投资机构的业务需求，定制了PEPM FOF版</p>
					</h3>
				</div>
			</div>
			<div class="index_fof_list">
				<ul>
					<li>
						<div class="index_fof_list_content">
							<div class="index_fof_list_left">
								<img src="images/icon-1.png">
							</div>
							<div class="index_fof_list_right">
								<div class="index_fof_list_right_title">
									<p> 母基金投资组合分析</p>
								</div>
								<div class="index_fof_list_right_con">
									<p>分析单支基金和母基金整体投资组合的情况...</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="index_fof_list_content">
							<div class="index_fof_list_left">
								<img src="images/icon-2.png">
							</div>
							<div class="index_fof_list_right">
								<div class="index_fof_list_right_title">
									<p> PE基金评估</p>
								</div>
								<div class="index_fof_list_right_con">
									<p>基金投资概况：投资进度，时间，价值，回报...</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="index_fof_list_content">
							<div class="index_fof_list_left">
								<img src="images/icon-3.png">
							</div>
							<div class="index_fof_list_right">
								<div class="index_fof_list_right_title">
									<p> 现金流管理</p>
								</div>
								<div class="index_fof_list_right_con">
									<p>安装一年12个月部署出资计划实际出资和回收现金流管理...</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="index_fof_list_content">
							<div class="index_fof_list_left">
								<img src="images/icon-4.png">
							</div>
							<div class="index_fof_list_right">
								<div class="index_fof_list_right_title">
									<p>子基金自助填报系统</p>
								</div>
								<div class="index_fof_list_right_con">
									<p>子基金可以通过互联网登录子基金门户...</p>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="index_fof_list_content">
							<div class="index_fof_list_left">
								<img src="images/icon-5.png">
							</div>
							<div class="index_fof_list_right">
								<div class="index_fof_list_right_title">
									<p> PE基金数据库</p>
								</div>
								<div class="index_fof_list_right_con">
									<p>3000家国内活跃的PE基金数据库2000名知名投资人档案...</p>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="index_fof_more">
				<a href=""><p>申请试用》</p></a>
			</div>
		</div>
	</div>
	<div class="index_core">
		<div class="index_core_txt">
			<div class="index_core_title">
				<h2>核心功能模块</h2>
			</div>
			<div class="index_xian index_xian_core"></div>
			<div class="index_core_con">
				<h3>
					<p>让系统真正成为投资人</p>
					<p>移动办公和打单的利器</p>
				</h3>
			</div>
		</div>
		<div class="index_core_table">
			<div class="swiper-container index_core_table_card">
			    <div class="swiper-wrapper index_core_table_card_list">
			      <div class="swiper-slide" style="background-image:url(images/img_5.png)"></div>
			      <div class="swiper-slide" style="background-image:url(images/img_5.png)"></div>
			      <div class="swiper-slide" style="background-image:url(images/img_5.png)"></div>
			      <div class="swiper-slide" style="background-image:url(images/img_5.png)"></div>
			    </div>
			    <div class="swiper-pagination"></div>
			</div>
		</div>
		<div class="index_core_more index_more">
			<a href=""><p>查看详情 》</p></a>
		</div>
	</div>
	<div class="index_case gray_back">
		<div class="index_case_txt">
			<div class="index_case_txt_title">
				<h2>案例</h2>
			</div>
			<div class="index_case_txt_xian index_xian"></div>
			<div class="index_case_txt_con">
				<h3>
					<p>让系统真正成为投资人</p>
					<p>移动办公和打单的利器</p>
				</h3>
			</div>
		</div>
		<div class="index_case_list">
			<ul>
				<li>
					<div class="index_case_list_content">
						<div class="index_case_list_imgbox">
							<img src="images/img_2.png">
						</div>
						<div class="index_case_list_txt">
							<p>让系统真正成为投资人让系统真正成为投资人让系统真正成为投资人</p>
						</div>
					</div>
				</li>
				<li>
					<div class="index_case_list_content">
						<div class="index_case_list_imgbox">
							<img src="images/img_2.png">
						</div>
						<div class="index_case_list_txt">
							<p>让系统真正成为投资人让系统真正成为投资人让系统真正成为投资人</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="index_case_more index_more">
			<a href=""><p>查看详情 》</p></a>
		</div>
	</div>
	<div class="index_customer">
		<div class="index_customer_txt">
			<div class="index_customer_txt_title">
				<h2>部分客户</h2>
			</div>
			<div class="index_customer_txt_xian index_xian"></div>
			<div class="index_customer_txt_con">
				<h3>
					<p>我们正在见证以支撑用户高效完</p>
					<p>成工作为目的全新一代企业应用的诞生</p>
				</h3>
			</div>
		</div>
		<div class="index_customer_table">
			<div class="swiper-container index_customer_table_card">
			    <div class="swiper-wrapper">
			      <div class="swiper-slide index_customer_table_card_con">
			      	<div class="index_customer_table_card_box gray_back">
			      		<div class="index_customer_table_card_img">
			      			<img src="images/logo-2.png">
			      		</div>
			      		<div class="index_customer_table_card_txt">
			      			<div class="index_customer_table_card_txt_title">
			      				<p>水木资产</p>
			      			</div>
			      			<div class="index_customer_table_card_txt_con">
			      				<p>
			      					依托清华大学资源和人脉网络构建的具备创新精神的专业资产管理机构
			      				</p>
			      			</div>
			      		</div>
			      	</div>
			      </div>
			      <div class="swiper-slide index_customer_table_card_con">
			      	<div class="index_customer_table_card_box  gray_back">
			      		<div class="index_customer_table_card_img">
			      			<img src="images/logo-2.png">
			      		</div>
			      		<div class="index_customer_table_card_txt">
			      			<div class="index_customer_table_card_txt_title">
			      				<p>水木资产</p>
			      			</div>
			      			<div class="index_customer_table_card_txt_con">
			      				<p>
			      					依托清华大学资源和人脉网络构建的具备创新精神的专业资产管理机构
			      				</p>
			      			</div>
			      		</div>
			      	</div>
			      </div>
			      <div class="swiper-slide index_customer_table_card_con">
			      	<div class="index_customer_table_card_box  gray_back">
			      		<div class="index_customer_table_card_img">
			      			<img src="images/logo-2.png">
			      		</div>
			      		<div class="index_customer_table_card_txt">
			      			<div class="index_customer_table_card_txt_title">
			      				<p>水木资产</p>
			      			</div>
			      			<div class="index_customer_table_card_txt_con">
			      				<p>
			      					依托清华大学资源和人脉网络构建的具备创新精神的专业资产管理机构
			      				</p>
			      			</div>
			      		</div>
			      	</div>
			      </div>
			    </div>
			    <!-- Add Arrows -->
			    <div class="swiper-button-next"></div>
			    <div class="swiper-button-prev"></div>
			</div>
			<div class="index_customer_table_iamge">
				<img src="images/img_3.png">
			</div>
		</div>
	</div>