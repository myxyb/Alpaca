<!DOCTYPE html>
<html>
<head>
	<title>pepm</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/pepm.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <script src="js/swiper.min.js"></script>
    <style>
    /*swiper*/
    .index_core_table_card {
      width: 100%;
      padding-top: 40px;
      padding-bottom: 44px;
      height: 345px;
    }
    .index_core_table_card_list > .swiper-slide {
      background-position: center;
      background-size: cover;
      width: 300px;
      height: 300px;
      box-shadow:0 0 10px 0 #000000;
    }
    .swiper-pagination{
    	width: 200px !important;
    	overflow:visible !important;
    	height: 40px;
    }
    .swiper-pagination p{
    	font-size: 33px !important;
    	color: #353535 !important;
    }
    .swiper-pagination-bullet{
    	background-color:transparent !important;
    }
    .swiper-pagination-bullet-active-main{
    	display: block !important;
    	left: 40px 	!important;
    }

    /*custmer*/
    .index_customer_table_card {
      width: 100%;
      height: 100%;
    }
    .index_customer_table_card_con {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
    .index_customer_table_card > .swiper-wrapper{
    	height: 220px;
    }
    .index_customer_table_card > .swiper-button-next{
    	background-image: url(images/right.png) !important;
    	margin-right: 15%;
    	width: 30px;
    }
    .index_customer_table_card > .swiper-button-prev{
    	background-image: url(images/left.png) !important;
    	margin-left: 15%;
    	width: 30px;
    }
  </style>
</head>
<body>
	<div class="m_head">
		<div class="m_logo">
			<a href="/"><img src="images/logo-1.png"></a>
		</div>
		<div class="nav_box_m">
			<div class="nav_m">
				<ul class="nav_list">
					<li><a href="index.html" class="nav_list_1 nav_list_on"><span>首页</span></a></li>
					<li><a href="module.html" class="nav_list_2"><span>功能模块</span></a></li>
					<li><a href="case.html" class="nav_list_3"><span>案例</span></a></li>
					<li><a href="about.html" class="nav_list_4"><span>关于我们</span></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="head gray_back">
		<div class="head_con">
			<div class="head_logo">
				<img src="images/logo-1.png">
			</div>
			<div class="nav_box">
				<div class="nav">
					<ul class="nav_list_p">
						<li><a href="index.html" class="nav_list_p_1 nav_list_p_on"><span>首页</span></a></li>
						<li><a href="module.html" class="nav_list_p_2"><span>功能模块</span></a></li>
						<li><a href="case.html" class="nav_list_p_3"><span>案例</span></a></li>
						<li><a href="about.html" class="nav_list_p_4"><span>关于我们</span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>