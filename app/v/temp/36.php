<!DOCTYPE html>
<html>
<head>
	<title><?=$page_title?$page_title:$title?> - PEPM</title>
	<meta name="keywords" content="<?=$meta_keywords?$meta_keywords:$title?>" />
    <meta name="description" content="<?=$meta_description?$meta_description:$title?>" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/static/ceshi/css/pepm.css?v=<?=date("Ymd",time())?>">
    <link rel="stylesheet" href="/static/ceshi/css/swiper.min.css?v=<?=date("Ymd",time())?>">
    <link href="/static/fav.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <script src="/static/ceshi/js/swiper.min.js?v=<?=date("Ymd",time())?>"></script>
    <script src="/static/ceshi/layui/layui.all.js?v=<?=date("Ymd",time())?>"></script>
    <link rel="stylesheet" href="/static/ceshi/layui/css/layui.css?v=<?=date("Ymd",time())?>">
    <style>
    /*swiper*/
    .index_core_table_card {
      width: 100%;
      padding-top: 40px;
      padding-bottom: 44px;
      height: 345px;
    }
    .index_core_table_card_list > .swiper-slide {
      background-position: center;
      background-size: 100%;
      background-repeat: no-repeat;
      width: 300px;
      height: 300px;
      box-shadow:0 0 10px 0 #000000;
    }
    .core_dian{
    	width: 200px !important;
    	overflow:visible !important;
    	height: 40px;
    }
    .core_dian p{
    	font-size: 33px !important;
    	color: #353535 !important;
    }
    .core_dian span{
    	background-color:transparent !important;
    }
    .swiper-pagination-bullet-active-main{
    	display: block !important;
    	left: 40px 	!important;
    }
    .core_dian > .swiper-pagination-bullet{
    	width: 0;
    }

    /*custmer*/
    .index_customer_table_card {
      width: 100%;
      height: 100%;
    }
    .index_customer_table_card_con {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
    .index_customer_table_card > .swiper-wrapper{
    	height: 220px;
    }
    .index_customer_table_card > .swiper-button-next{
    	/*background-image: url(images/right.png) !important;*/
    	margin-right: 15%;
    	width: 30px;
    }
    .index_customer_table_card > .swiper-button-prev{
    	/*background-image: url(images/left.png) !important;*/
    	margin-left: 15%;
    	width: 30px;
    }
    @media screen and (max-width: 768px) {
    	.index_customer_table_card > .swiper-button-next{
    		margin-right: 4%;
    	}
    	.index_customer_table_card > .swiper-button-prev{
    		margin-left: 4%;
    	}
    }
  </style>
</head>
<body>
	<div class="head gray_back">
		<div class="head_con layui-row">
			<div class="head_logo layui-col-md4 layui-col-sm12 layui-col-xs12">
				<img src="/static/ceshi/images/logo-1.png">
			</div>
			<div class="nav_box layui-col-md8 layui-col-sm12 layui-col-xs12">
				<div class="nav">
					<ul class="nav_list_p">
						<?=alpa("daohang","daohang")?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- end_head -->
	<?=$al_content?>
	<!-- footer -->
	<div class="black">
		<div class="footer">
		<div class="footer_con">
			<div class="footer_con_title">
				<p>中国领先的FOF投资管理软件</p>
			</div>
			<div class="footer_link layui-row">
				<div class="footer_con_left layui-col-md6">
					<div class="footer_con_left_img">
						<img src="/static/ceshi/images/img_4.png">
					</div>
					<div class="footer_con_left_con">
						<p>扫码申请试用</p>
					</div>
				</div>
				<div class="footer_con_right_al layui-col-md6">
					<?=alpa("link","link")?>
				</div>

			</div>
		</div>
		<div class="footer_xian index_xian"></div>
		<div class="footer_contact">
			<p>江苏省苏州市中国苏州创业园4号楼</p>
			<p>0512-68075637</p>
			<p>www.pepm.com.cn</p>
		</div>
		</div>
	</div>
	<script>
    var swiper = new Swiper('.index_core_table_card', {
	    effect: 'coverflow',
	    grabCursor: true,
	    centeredSlides: true,
	    slidesPerView: 'auto',
	    coverflowEffect: {
	      rotate: 50,
	      stretch: 0,
	      depth: 100,
	      modifier: 1,
	      slideShadows : true,
	    },
	    pagination: {
		    el: '.swiper-pagination',
		    clickable: true,
		    dynamicBullets: true,
    		dynamicMainBullets: 1,
		  renderBullet: function (index, className) {
		      switch(index){
		        case 0:text='数据管理';break;
		        case 1:text='在线申报';break;
		        case 2:text='流程审批';break;
		        case 3:text='现金流';break;
		        case 4:text='LP门户';break;
            case 5:text='生成报告';break;
            case 6:text='行业数据';break;
            case 7:text='统计图表';break;
		      }
		      return '<div class="' + className + '" style="display:none" ><p>'+text+'</p></div>';
		    },
		},
    });
    var swiper = new Swiper('.index_customer_table_card', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
</body>
</html>