<div class="content">
<div class="con">
  <form class="form-horizontal layui-form" id="editform" method="POST" >

    <div class="layui-form-item">
      <label class="layui-form-label">空间名：</label>
      <div class="layui-input-block input_text">
        <input type="text" name="name" required value="<?=$sites['0']['name']?>"  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">空间目录：</label>
      <div class="layui-input-block input_text">
        <input type="text" name="domain" required value="<?=$sites['0']['domain']?>"  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <div class="layui-input-block">
        <button class="layui-btn form_btn" lay-submit lay-filter="formDemo">立即提交</button>
        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
      </div>
    </div>
  </form>
</div>
</div>
<style>
  body{
    background-color: #f6f6f6 !important;
  }
</style>