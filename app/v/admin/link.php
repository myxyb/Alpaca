
<div class="pull-right" >
  <a href="?/admin/link/add/<?=$upid?>/" class="btn ions" ><i class="icon ion-md-add-circle"></i> 添加链接 </a>
</div>
<div class="breadcrumb">
  <span class="layui-breadcrumb crumbs">
    <a href="?/admin/link/"><?=empty(seg(3))?"<cite>":""?>链接</a>
  </span>
</div>
<table class="table layui-table">
  <thead>
    <tr>
      <th width="5" >#</th><th>名称</th><th>更新日期</th><th>操作</th>
    </tr>
  </thead>
  <?foreach($records as $r){?>
  <tr id="r<?=$r['id']?>">
    <td><input type="checkbox" ></td><td><?=$r['name']?></td>
    <td><?=date('Y-m-d',$r['update_date'])?></td>
    <td>
      <a href="?/admin/link/edit/<?=$r['id']?>/" ><img src="/static/edit.png"></a>
      <a href="javascript:bdel('?/admin/link/del/<?=$r['id']?>/', 'r<?=$r['id']?>');" ><img src="/static/del.png"></a>
    </td>
  </tr>
  <?}?>
</table>
<style type="text/css">
  .ion-md-create{
    padding-right: 5px;
  }
</style>
