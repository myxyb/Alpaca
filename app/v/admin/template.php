<!DOCTYPE html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title><?=$page_title?> Alpaca CMS </title>
  <meta name="keywords" content="<?=$meta_keywords?>"/>
  <meta name="description" content="<?=$meta_description?>"/>
  <link href="https://unpkg.com/ionicons@4.4.6/dist/css/ionicons.min.css" rel="stylesheet">
  <link href="static/alpaca.css" rel="stylesheet">
  <script charset="utf-8" src="static/js/jquery.js"></script>
  <script charset="utf-8" src="static/js/alpaca.js"></script>
</head>
<body>
  <div class="wrap">
    <ul class="nav layui-nav " id="nav" >
      <?foreach($menu as $url=>$m) {?>
      <li  class="nav-item layui-nav-item <?=($url==seg(2))?'layui-this':''?>" ><a class="nav-link " href="<?=(($url == 'logout') ? '?/':'?/admin/').$url?>/"><?=$m?></a></li><?}?>
    </ul>
    <div class="gray">
      <?=$al_content?>
    </div>
    
  </div>
</body>
</html>
<script>
//注意：导航 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
  
  //…
});

layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;

  form.on('select(type)',function(data){
    var y=data.value;
    if(y=="list"){
      $(".right_box_son").removeClass("disnone_son");
    }else if(y=="form"){
      $(".right_box_son").addClass("disnone_son");
    }
  });  
  form.on('select(model)',function(data){
    var y=data.elem;
    if(y!=""){
      window.location.href = "?/admin/"+y.dataset.type+"/"+y.dataset.url+"/"+y.dataset.upid+"/"+y.value;
    }
  });  
});
</script>
<style>
  .crumbs a:hover{
    color: #aa0000!important;
  }
</style>
