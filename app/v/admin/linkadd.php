<div class="content">
<div class="con">
<form class="form-horizontal" method="POST" >
  <div class="layui-form-item">
      <label class="layui-form-label">页面名称：</label>
      <div class="layui-input-block input_text">
        <input type="text" name="name" required value="<?=$page['name']?>"  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
        <input type="hidden" name="type" value="link" required >
        <input type="hidden" name="sid" value="" required >
      </div>
    </div>
  <table class="table layui-table" >
    <thead>
    <tr class="th" >
      <th>链接名称</th><th>url地址</th><th>排序</th><th>新窗口 <a title="选中后，链接将在新页面打开" class="tip" >?</a></th><th></th>
    </tr>
    </thead>
    
    <?foreach($ext as $info){
      $j++;?>
      <tr  class="odd" >
          <td> <input type="text" name="info[<?=$j?>][title]" value="<?=$info['title']?>" /> </td> 
          <td> <input type="text" size="30" name="info[<?=$j?>][name]" value="<?=$info['name']?>"  /></td> 
          <td> <input type="text" size="4" name="info[<?=$j?>][order]" value="<?=$info['order']?>"  /></td> 
          <td> <input name="info[<?=$j?>][blank]" value="1" type="checkbox" <?=$info['blank']?'checked':''?> /></td>
          <td> <a href="javascript:void(0);" onclick="wipeout($(this));" ><img src="/static/del.png"></a></td>
     </tr>
    <?}?>
    
    <?for($i=$j+1;$i<$j+6;$i++){?>
    <tr  class="odd" >
          <td> <input type="text" name="info[<?=$i?>][title]" value="" /> </td> 
          <td> <input type="text" size="30" name="info[<?=$i?>][name]" value=""  /></td> 
          <td> <input type="text" size="4" name="info[<?=$i?>][order]" value=""  /></td> 
          <td> <input name="info[<?=$i?>][blank]" value="1" type="checkbox"  /></td>
          <td> <a href="javascript:void(0);" onclick="wipeout($(this));" ><img src="/static/del.png"></a></td>
     </tr>
    <?}?>
  </table>
  
  <div class="layui-form-item">
      <div class="layui-input-block link_btn">
        <button class="layui-btn form_btn" lay-submit lay-filter="formDemo">保存</button>
        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
      </div>
    </div>
</form>
</div>
</div>
