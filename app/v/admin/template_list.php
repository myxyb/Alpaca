<?php 
  $now_title="";
  if(seg(2)=="template"){
    $now_title="模板";
  }else if(seg(2)=="layout"){
    $now_title="排版";
  }else if(seg(2)=="model"){
    $now_title="模型";
  } 
?>
<div class="pull-right" >
  <a href="?/admin/<?=$type?>/add/<?=$upid?>/" class="btn ions" ><i class="icon ion-md-add-circle"></i> 添加<?=$now_title?> </a>
</div>
<div class="breadcrumb">
  <span class="layui-breadcrumb crumbs">
    <a href="?/admin/<?=seg(2)?>/"><?=empty(seg(3))?"<cite>":""?><?=$now_title?></a>
  </span>
</div>

<table class="table table-striped layui-table">
  <thead>
    <tr>
      <th width="5" >#</th><th>名称</th><th>更新日期</th> <th>操作</th>
    </tr>
  </thead>
  <?foreach($records as $r){?>
  <tr id="r<?=$r['id']?>" title="line<?=++$i?>">
    <td><input type="checkbox" ></td>
    <td> <a href="?/admin/<?=$type?>/edit/<?=$r['id']?>/"><img src="/static/edit_til.png"> <?=$r['name']?></a></td>
    <td><?=date('Y-m-d',$r['update_date'])?></td> 
    <td class="ions" >
      <a href="?/admin/<?=$type?>/edit/<?=$r['id']?>/" ><img src="/static/edit.png"></a>
      <a href="javascript:bdel('?/admin/page/del/<?=$r['id']?>/', 'r<?=$r['id']?>');" ><img src="/static/del.png"></a>
    </td>
  </tr>
  <?}?>
</table>

<script>
function order_up(id) {
  line = $('#r' + id).attr('title');
  lineno = line.substring(line.length - 1);
  if(lineno == 1) {
    alert('本文章已经为第一篇！');
  } else {
    $.get('?/admin/order_edit/' + id + '/up', function(data){
      elem_change('line' + lineno, 'line' + (parseInt(lineno) - 1));
    });
  }
}

function order_down(id) {
  line = $('#r' + id).attr('title');
  lineno = line.substring(line.length - 1);
  if(lineno == <?=$i?>) {
    alert('本文章已经为最后一篇！');
  } else {
    $.get('?/admin/order_edit/' + id + '/down', function(data){
      elem_change('line' + (parseInt(lineno) + 1), 'line' + lineno);
    });
  }
}

function elem_change(elem1, elem2) {
  line1 = $('tr[title='+elem1+']');
  id1 = line1.attr('id');
  line2 = $('tr[title='+elem2+']');
  id2 = line2.attr('id');
  temp = line1.html();
  line1.html(line2.html());
  line2.html(temp);
  line1.attr('id', id2);
  line2.attr('id', id1);
}
</script>