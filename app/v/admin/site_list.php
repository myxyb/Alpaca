<div class="pull-right" >
  <a href="?/admin/site/add/" class="btn ions" ><i class="icon ion-md-add-circle"></i> 新增空间 </a>
</div>
<div class="breadcrumb">
  <span class="layui-breadcrumb crumbs">
    <a href="?/admin/site/"><?=empty(seg(3))?"<cite>":""?>空间</a>
  </span>
</div>
<table class="table layui-table">
  <thead>
    <tr>
      <th width="5" >#</th><th>空间名</th><th>空间目录</th><th>操作</th>
    </tr>
  </thead>
  <?foreach($sites as $r) {?>
  <tr id="<?=$r['id']?>">
    <td><input type="checkbox" ></td><td><?=$r['name']?></td>
    <td><?=$r['domain']?></td>
    <td>
      <a href="?/admin/site/0/<?=$r['uuid']?>/"><img style="padding-right: 5px;" src="/static/qiehuan.png">切换</a>
    </td>
  </tr>
  <?}?>
</table>
