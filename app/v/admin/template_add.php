<div class="content">
<div class="con">

<form class="form-horizontal layui-form" id="editform" method="POST" >

    <!-- <?=$writable ? '':'<div class="alert">当前文件处于不可写状态，请检查文件所在目录权限！</div>'?> -->

  <div class="layui-form-item">
      <label class="layui-form-label">名称：</label>
      <div class="layui-input-block input_text">
        <input type="text" name="name" required value="<?=$page['name']?>"  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
      <input type="hidden" name="type" value="<?=$page['type']?>" required >
      <input type="hidden" name="upid" value="<?=$page['upid']?>" >
      <input type="hidden" name="sid" value="<?=$page['sid']?>" >
      </div>
    </div>

  <div class="layui-form-item">
    <label class="layui-form-label" >内容：</label> 
    <input type="hidden" id="content" name="content" value="" >
    <pre id="htmleditor"><?=htmlspecialchars($page['content'])?></pre>
  </div>
  
  <div class="layui-form-item">
      <div class="layui-input-block">
        <button class="layui-btn form_btn" lay-submit lay-filter="formDemo">立即提交</button>
        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
      </div>
    </div>
</form>
</div>
</div>
<script src="static/plugins/ace/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
  var editor = ace.edit("htmleditor");
  editor.setTheme("ace/theme/textmate");
editor.setFontSize(16);
  editor.getSession().setMode("ace/mode/php");
  $('#editform').submit(function(){
    $("#content").val(editor.getValue());});
</script>
<style>
  body{
    background: #f6f6f6 !important;
  }
</style>