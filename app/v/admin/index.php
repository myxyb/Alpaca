<div class="pull-right" >
  <a href="?/admin/page/add/<?=$upid?>/" class="btn ions" ><i class="icon ion-md-add-circle"></i> 添加页面 </a>
</div>
<div class="breadcrumb">
  <span class="layui-breadcrumb crumbs">
    <a href="?/admin/page/"><?=empty(seg(3))?"<cite>":""?>页面</a>
    <?php foreach ($uper as $k => $v) { ?>
      <a href="?/admin/page/<?=$k?>"><?=$k==seg(3)?"<cite>":""?><?=$v?></a>
    <?php } ?>
  </span>
</div>


<table class="table table-striped layui-table">
  <thead>
    <tr>
      <th width="5" >#</th><th>标题</th><th>页面</th><th>更新日期</th><th>排序</th><th>操作</th>
    </tr>
  </thead>
  <?foreach($records as $r){?>
  <?php if($r['ext1']['type']=="list"): ?>
  <tr id="r<?=$r['id']?>" title="line<?=++$i?>">
    <td><input type="checkbox" ></td>
    <td> <img src="/static/wenjian.png"> <?=$r['title']?></td>
    <td><a href="?/admin/page/edit/<?=$r['id']?>/"><img src="/static/edit_til.png"> <?=$r['name']?></a> </td>
    <td><?=date('Y-m-d',$r['update_date'])?></td>
    <td><input type="text" size="5" data-id="<?=$r['id']?>" value="<?=$r['order']?>" onchange="change_order(this)" /></td>
    <td class="ions" >
      <!-- <a href="javascript:order_up(<?=$r['id']?>);"><i class="icon-arrow-up"></i></a>
      <a href="javascript:order_down(<?=$r['id']?>);"><i class="icon-arrow-down"></i></a> -->
      <!-- <a href="?/home/page/<?=$r['name']?>/" target="_blank" ><i class="icon-search"></i></a> -->
      <a href="?/admin/page/<?=$r['id']?>/" ><img src="/static/wenjian.png"></a>
      <a href="?/admin/page/edit/<?=$r['id']?>/" ><img src="/static/edit.png"></a>
      <a href="javascript:bdel('?/admin/page/del/<?=$r['id']?>/', 'r<?=$r['id']?>');" ><img src="/static/del.png"></a>
    </td>
  </tr>
  <?php endif ?>
  <?}?>
  <?foreach($records as $r){?>
  <?php if($r['ext1']['type']!="list"): ?>
  <tr id="r<?=$r['id']?>" title="line<?=++$i?>">
    <td><input type="checkbox" ></td>
    <td>  <?=$r['title']?></td>
    <td><a href="?/admin/page/edit/<?=$r['id']?>/"><img src="/static/edit_til.png"> <?=$r['name']?></a> </td>
    <td><?=date('Y-m-d',$r['update_date'])?></td>
    <td><input type="text" size="5"  data-id="<?=$r['id']?>" value="<?=$r['order']?>" onchange="change_order(this)" /></td>
    <td class="ions" >
      <!-- <a href="javascript:order_up(<?=$r['id']?>);"><i class="icon-arrow-up"></i></a>
      <a href="javascript:order_down(<?=$r['id']?>);"><i class="icon-arrow-down"></i></a> -->
      <!-- <a href="?/home/page/<?=$r['name']?>/" target="_blank" ><i class="icon-search"></i></a> -->
      <!-- <a href="?/admin/page/<?=$r['id']?>/" ></a> -->
      <a href="?/admin/page/edit/<?=$r['id']?>/" ><img src="/static/edit.png"></a>
      <a href="javascript:bdel('?/admin/page/del/<?=$r['id']?>/', 'r<?=$r['id']?>');" ><img src="/static/del.png"></a>
    </td>
  </tr>
  <?php endif ?>
  <?}?>
</table>

<script>
function order_up(id) {
  line = $('#r' + id).attr('title');
  lineno = line.substring(line.length - 1);
  if(lineno == 1) {
    alert('本文章已经为第一篇！');
  } else {
    $.get('?/admin/order_edit/' + id + '/up', function(data){
      elem_change('line' + lineno, 'line' + (parseInt(lineno) - 1));
    });
  }
}

function order_down(id) {
  line = $('#r' + id).attr('title');
  lineno = line.substring(line.length - 1);
  if(lineno == <?=$i?>) {
    alert('本文章已经为最后一篇！');
  } else {
    $.get('?/admin/order_edit/' + id + '/down', function(data){
      elem_change('line' + (parseInt(lineno) + 1), 'line' + lineno);
    });
  }
}

function elem_change(elem1, elem2) {
  line1 = $('tr[title='+elem1+']');
  id1 = line1.attr('id');
  line2 = $('tr[title='+elem2+']');
  id2 = line2.attr('id');
  temp = line1.html();
  line1.html(line2.html());
  line2.html(temp);
  line1.attr('id', id2);
  line2.attr('id', id1);
}

function change_order(a){
  $.ajax({  
      type:'POST',  
      url: '?/admin/change_order/',
      data : {id:a.dataset.id,order:a.value}
  });

}
</script>