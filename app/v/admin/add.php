<script charset="utf-8" src="static/kindeditor-min.js"></script>
<script>
  var editor;
  KindEditor.ready(function(K) {
    editor = K.create('textarea[name="content"]', {
      resizeType : 1,
      cssPath : 'static/pepm.css',
      width:'100%',
      allowPreviewEmoticons : false,
      allowImageUpload : true,
      uploadJson : '/admin/upload',
    });
  });
</script>
<div class="content">
<div class="con">
<div class="breadcrumb">
  <span class="layui-breadcrumb crumbs">
    <a href="?/admin/page/"><?=empty($uper_id)?"<cite>":""?>页面</a>
    <?php foreach ($uper_id as $k => $v) { ?>
      <a href="?/admin/page/<?=$k?>"><?=$v==end($uper_id)?"<cite>":""?><?=$v?></a>
    <?php } ?>
  </span>
</div>
<hr>
<div class="layui-row">
<form class="form-horizontal layui-form" method="POST" enctype="multipart/form-data">
<!-- 页面左侧 -->
<div class="add_left layui-col-md9">
  <div class="seg">
    <div class="seg_title">
      <span>常规信息</span>
    </div>
  </div>
  <div class="con_box">
    <div class="layui-form-item">
        <label class="layui-form-label">页面名称：</label>
        <div class="layui-input-block">
          <input type="text" name="name" required value="<?=$page['name']?>"  lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
        <input type="hidden" name="type" value="page" required >
        <input type="hidden" name="upid" value="<?=$page['upid']?>" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">页面标题：</label>
        <div class="layui-input-block">
          <input type="text" name="title" required value="<?=$page['title']?>"  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">网页内容：</label> 
      <div class="controls" style="margin-left: 85px;">
        <textarea name="content" rows="20" ><?=htmlspecialchars($page['content'])?></textarea>
      </div>
    </div>
  </div>
  <!-- 模型参数 -->
  <div class="seg <?=$ext1['model']!='no'?'':'disnone_pro'?>">
    <div class="seg_title">
      <span>模型参数</span>
    </div>
  </div>
  <div class="con_box <?=$ext1['model']!='no'?'':'disnone_pro'?>">
    <div class="more_fun_con">
      <!-- 模型菜单 -->
      <div>
        <?php foreach ($model_con as $k => $v) { ?>
          <?php $k = "model_".$k; ?>
          <?php if($v['type']=="text"){ ?>
          <div class="layui-form-item">
              <label class="layui-form-label"><?=$v['name']?>：</label>
              <div class="layui-input-block">
                <input type="text" name="ext1[<?=$k?>]" value="<?=$ext1[$k]?>" autocomplete="off" class="layui-input">
              </div>
          </div>
          <?php }else if($v['type']=="content"){ ?>
          <div class="layui-form-item layui-form-text">
            <label class="layui-form-label"><?=$v['name']?>：</label>
            <div class="layui-input-block">
              <textarea name="ext1[<?=$k?>]" placeholder="请输入内容" class="layui-textarea"><?=htmlspecialchars($ext1[$k])?></textarea>
            </div>
          </div>
          <?php }else if ($v['type']=="extension") {?>
          <div class="layui-form-item layui-form-text">
            <label class="layui-form-label"><?=$v['name']?>：</label>
            <div class="layui-input-block">
              <textarea name="ext1[<?=$k?>]" placeholder="请输入内容" class="layui-textarea"><?=htmlspecialchars($ext1[$k])?></textarea>
            </div>
          </div>
          <?php }else if ($v['type']=="file") {?>

          <div class="layui-form-item layui-form-text" style="margin-bottom: 25px;">
            <label class="layui-form-label"><?=$v['name']?>：</label>
            <div class="controls" style="padding-top: 4px;margin-left: 85px;">
              <div class="file" id="file1_<?=$k?>">
                <input type="file" name="<?=$k?>" value="请选择图片">
                <div class="fileerrorTip1_<?=$k?>" id="<?=$k?>_name"><?=$ext1[$k]['name']?></div>
                <div class="showFileName1_<?=$k?>"></div>
              </div>
              <script type="text/javascript">
                $("#file1_<?=$k?>").on("change","input[type='file']",function(){
                  var filePath=$(this).val();
                         $(".fileerrorTip1_<?=$k?>").html("").hide();
                         var arr=filePath.split('\\');
                         var fileName=arr[arr.length-1];
                         $(".showFileName1_<?=$k?>").html(fileName);
                 });
              </script>
              <div class="image_box <?=$ext1[$k]['name']==''?'disnone_pro':''?>" style="margin-top: 15px;" id="<?=$k?>">
              <input type="hidden"  name="ext1[<?=$k?>][name]" value="<?=$ext1[$k]['name']?>">
              <input type="hidden"  name="ext1[<?=$k?>][url]" value="<?=$ext1[$k]['url']?>">
              <img src="<?=$ext1[$k]['url']?>" style="padding-top: 15px;max-width: 100px;">
              <a href="javascript:delImage('<?=$page["id"]?>','<?=$k?>','<?=$page["sid"]?>');" style="position: absolute;"><img src="/static/close.png" style=" width:15px; height:15px;position:relative; z-index:2; top:8px;right:8px;<?=empty($ext1[$k]['url'])?"display: none;":""?>"></a>
              </div>
            </div>
          </div>
          <?php } ?>
        <?php } ?>
      </div>
    </div>
    <script type="text/javascript">
      function delImage(id,model,sid){
        $.post('/?/admin/delimage/'+id+'/'+model+'/'+sid,function(data){
            $('#'+model).remove();
            $('#'+model+"_name").remove();
        });
      }
    </script>
  </div>
</div>
<!-- 页面右侧 -->
<div class="add_right layui-col-md3">
  <!-- seo设置 -->
  <div class="right_box">
    <div class="seg">
      <div class="seg_title">
        <span>seo设置</span>
      </div>
    </div>
    <div class="con_box_right">
    <div class="layui-form-item">
      <label class="layui-form-label">关键词：</label>
      <div class="layui-input-block seo_input">
        <input type="text" name="ext1[<?=$ext?>]" value="<?=$ext1[$ext]?>" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">描述：</label>
      <div class="layui-input-block seo_input">
        <input type="text" name="ext1[<?=$ext?>]" value="<?=$ext1[$ext]?>" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">页面标题：</label>
      <div class="layui-input-block seo_input">
        <input type="text" name="ext1[<?=$ext?>]" value="<?=$ext1[$ext]?>" autocomplete="off" class="layui-input">
      </div>
    </div>
    </div>
  </div>
  <!-- 高级功能选择 -->
  <div class="right_box">
    <div class="seg">
      <div class="seg_title">
        <span>高级功能</span>
      </div>
    </div>
    <div class="con_box_right">
    <div class="more_fun_con  disnone">
      <div class="layui-form-item">
        <label class="layui-form-label" >风格文件：</label>
        <div class="layui-input-block">
          <select type="text" class="input-xxlarge input_select" name="ext1[template]"  >
            <option></option>
            <?foreach($templates as $template){?>
            <option  <?=$ext1['template']==$template['id']?'selected':''?> value="<?=$template['id']  ?>" ><?=$template['name']?></option>
            <?}?>
          </select>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label" >排版文件：</label>
        <div class="layui-input-block">
          <select type="text" class="input-xxlarge input_select" name="ext1[layout]"  >
            <option></option>
            <?foreach($layouts as $layout){?>
            <option  <?=$ext1['layout']==$layout['id']?'selected':''?> value="<?=$layout['id']  ?>" ><?=$layout['name']?></option>
            <?}?>
          </select>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label" >页面类型：</label>
        <div class="layui-input-block">
          <select type="text" id="more_son" lay-filter="type" class="input-xxlarge input_select" name="ext1[type]" onchange="func_son(this.id)">
            <option></option>
            <option  <?=$ext1['type']=='form'?'selected':''?> value="form" >页面</option>
            <option  <?=$ext1['type']=='list'?'selected':''?> value="list" >目录</option>
          </select>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label" >原型选择：</label>
        <div class="layui-input-block">
          <select type="text" id="more_pro" lay-filter="model" data-type="page" data-upid="<?=$mod['upid']?>" data-url="<?=$mod['url']?>" class="input-xxlarge input_select" name="ext1[model]" onchange="func_gro(this.id)">
            <option value="no">不使用</option>
            <?foreach($models as $model){?>
            <option  <?=$ext1['model']==$model['id']?'selected':''?> value="<?=$model['id']  ?>" ><?=$model['name']?></option>
            <?}?>
          </select>
        </div>
      </div>
    </div>
    </div>
  </div>
  <!-- 子页面设置 -->
  <div class="right_box right_box_son <?=$ext1['type']=='list'?'':'disnone_son'?>">
    <div class="seg">
      <div class="seg_title">
        <span>子页面设置</span>
      </div>
    </div>
    <div class="con_box_right">
    <div class="son_function">
      <div class="layui-form-item">
          <label class="layui-form-label">每页显示：</label>
          <div class="layui-input-block">
            <input type="text" name="ext1[son_number]" value="<?=$ext1['son_number']?>"  placeholder="请输入数量" autocomplete="off" class="layui-input">
          </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label" >风格文件：</label>
        <div class="layui-input-block">
          <select type="text" class="input-xxlarge input_select" name="ext1[son_template]"  >
            <option></option>
            <?foreach($templates as $son_template){?>
            <option  <?=$ext1['son_template']==$son_template['id']?'selected':''?> value="<?=$son_template['id']  ?>" ><?=$son_template['name']?></option>
            <?}?>
          </select>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label" >排版文件：</label>
        <div class="layui-input-block">
          <select type="text" class="input-xxlarge input_son_select" name="ext1[son_layout]">
            <option></option>
            <?foreach($layouts as $son_layout){?>
            <option  <?=$ext1['son_layout']==$son_layout['id']?'selected':''?> value="<?=$son_layout['id']  ?>" ><?=$son_layout['name']?></option>
            <?}?>
          </select>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label" >原型选择：</label>
        <div class="layui-input-block">
          <select type="text" class="input-xxlarge input_son_select" name="ext1[son_model]"  >
            <option></option>
            <?foreach($models as $son_model){?>
            <option  <?=$ext1['son_model']==$son_model['id']?'selected':''?> value="<?=$son_model['id']  ?>" ><?=$son_model['name']?></option>
            <?}?>
          </select>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<hr>
<div class="layui-form-item">
      <div class="layui-input-block" style="margin-left: 40%">
        <button class="layui-btn form_btn" lay-submit lay-filter="formDemo">立即提交</button>
        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
      </div>
  </div>
</div>
</form>
</div>

</div>
<style>
  .content{
    padding: 10px 20px 20px;
  }
  .seg{
    background-color: #aa0000;
    color: white;
    padding: 10px 20px;
    border-radius: 3px;
  }
  .seg_title{
    font-size: 16px;
  }
  .layui-form-label{
    padding: 9px 0px;
  }
  .disnone_pro,.disnone_son{
    display: none;
  }
  .con_box{
    padding: 18px 28px 3px 28px;
    background: #fcfcfc;
    margin-bottom: 10px;

  }
  .con_box_right{
    padding: 18px 10px 3px 10px;
    background: #fcfcfc;
    margin-bottom: 10px;
  }
  .layui-input-block{
    margin-left: 85px;
  }
  .image_box {
    margin-top: -5px !important;
  }
</style>
<script type="text/javascript">
  function aClick () {
     $(".disnone").removeClass("disnone");
  }
  // function func_son(a){
  //   var y=document.getElementById(a).value;
  //   if(y=="list"){
  //     $(".right_box_son").removeClass("disnone_son");
  //   }else if(y=="form"){
  //     $(".right_box_son").addClass("disnone_son");
  //   }
  // }
  function func_gro(a){
    var y=document.getElementById(a);
    if(y!=""){
      window.location.href = "?/admin/"+y.dataset.type+"/"+y.dataset.url+"/"+y.dataset.upid+"/"+y.value;
    }
  }
  $("#file1").on("change","input[type='file']",function(){
    var filePath=$(this).val();
    var a = $(this).dataset.type;
    console.log(a);
           $(".fileerrorTip1").html("").hide();
           var arr=filePath.split('\\');
           var fileName=arr[arr.length-1];
           $(".showFileName1").html(fileName);
   });
</script>
  
