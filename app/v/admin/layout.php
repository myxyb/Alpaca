<ul class="breadcrumb">
  <li class="active"><a href="?/admin/layout/">模版</a> <span class="divider">/</span></li>
</ul>

<table class="table">
  <thead>
    <tr>
      <th width="5" >#</th><th>文件名</th><th>大小</th><th>最后修改日期</th><th>操作</th>
    </tr>
  </thead>
  <?foreach($records as $r) {
    $fname = str_replace('.', '_', $r['name']);?>
  <tr id="<?=$fname?>">
    <td><input type="checkbox" ></td><td><a href="" ><?=$r['name']?></a></td>
    <td><?=number_format($r['size']/1000,2)?>k</td>
    <td><?=date('Y-m-d',$r['time'])?></td>
    <td>
      <a href="?/admin/layout_edit/<?=$r['name']?>/" ><i class="icon-edit"></i>edit </a>
      <a href="javascript:bdel('?/admin/layout_del/<?=$r['name']?>/', '<?=$fname?>');"><i class="icon-trash"></i></a>
    </td>
  </tr>
  <?}?>
</table>
