<style>
#editor{height:400px;}
</style>

<ul class="breadcrumb">
  <li><a href="?/admin/layout/">模版</a> <span class="divider">/</span></li>
  <li class="active" ><?=$layout?></li>
</ul>

<?=$writable ? '':'<div class="alert">当前文件处于不可写状态，请检查文件所在目录权限！</div>'?>

<pre id="editor"><?=htmlspecialchars($content)?></pre>
<div class="form-actions">
  <button type="button" class="btn btn-primary <?=$writable ? '':'disabled'?>" id="submit">保存修改</button>
</div>

<script src="static/plugins/ace/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
  var editor = ace.edit("editor");
  editor.setTheme("ace/theme/textmate");
  editor.getSession().setMode("ace/mode/php");
  
  $('#submit').click(function(){
    if(<?=$writable?>+'' == 1) {
      var content = editor.getValue();
      $.post('?/admin/layout_save/<?=$layout?>/', {content: content}, function(data) {
        if(data == '1') {
          window.location.href = "?/admin/redirect/编辑成功/";
        } else {
          alert(data);
        }
      });
    } else {
      return false;
    }
  });
</script>