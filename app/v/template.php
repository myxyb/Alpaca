<!DOCTYPE html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title><?=$page_title?></title>
  <meta name="keywords" content="<?=$meta_keywords?>"/>
  <meta name="description" content="<?=$meta_description?>"/>
  <link href="static/alpaca.css" rel="stylesheet"></head>
<body>
  <div class="container">
    <?=$al_content?>
  </div>
</body>
</html>
