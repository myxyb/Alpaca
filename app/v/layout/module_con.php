<div class="layui-tab layui-tab-brief">
    <ul class="layui-tab-title">
    <?php foreach ($lists as $k => $v) {?>
      <li class="<?=$k==0?'layui-this':''?>"><?=$v['title']?></li>
    <?php } ?>
    </ul>
    <div class="layui-tab-content module_tab_con">

    	<?php foreach ($lists as $k => $v) {?>
    		<div class="layui-tab-item <?=$k==0?'layui-show':''?>"> 
	          	<div class="module_content layui-row">
	          		<div class="module_con layui-col-md4 layui-col-sm4">
	          			<div class="module_title">
	          				<p><?=$v['ext1']['model_short_title']?></p>
	          			</div>
	          			<div class="index_xian module_xian"></div>
	          			<div class="module_txt">
	          				<p><?=$v['ext1']['model_describe']?></p>
	          			</div>
	          		</div>
	          		<div class="module_img_box layui-col-md8  layui-col-sm8">
	          			<div class="swiper-container">
					        <div class="swiper-wrapper">
					            <div class="swiper-slide module_img">
					                <?=$v['content']?>
					            </div>
					        </div>
					        <!-- Add Scroll Bar -->
					        <div class="swiper-scrollbar"></div>
					    </div>
	          		</div>
	          	</div>
	        </div>
    	<?php } ?>
    </div>
</div>