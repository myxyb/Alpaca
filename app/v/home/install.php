<style>
.server-info{padding-top: 5px;}
</style>

<h1>羊驼CMS 安装</h1>
<div class="well" >
<h3>注意，服务器环境需满足以下条件才可安装羊驼!</h3>  
<ul>
  <li>请确认 服务器安装url重写 url_rewrite 模块，并且已经开启 </li>
  <li>apache 支持 .htaccess 配置 </li>
  <li>确认php short_tag 选项为 on</li>
  <li>请确认 app/, upfile/ 目录为可写 </li>
  <li>后台地址为 ’安装网址/?/admin‘，默认用户名和密码均为admin </li>
</ul>
</div>
<form class="form-horizontal" method="POST" >
  <h4>服务器配置</h4>
  <div class="control-group">
    <label class="control-label">安装网址</label>
    <div class="controls">
      <input type="text" name="base_dir" value="<?=$base?>" /> <span class="help-inline">如： http://domain/alpaca/</span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">目录权限</label>
    <div class="controls">
      <p class="server-info <?=$writable ? 'text-info' : 'text-error'?>"><?=$writable ? '可写' : '不可写'?></p>
    </div>
  </div>
  <div class="control-group" style="margin-top:-15px;">
    <label class="control-label">是否支持url_rewrite</label>
    <div class="controls">
      <p class="server-info <?=$rewrite ? 'text-info' : 'text-error'?>"><?=$rewrite ? '已开启' : '未开启'?></p>
    </div>
  </div>
  
  <h4>数据库配置</h4>
  <div class="control-group">
    <label class="control-label">服务器</label>
    <div class="controls">
      <input type="text" name="host" value="localhost" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">用户名</label>
    <div class="controls">
      <input type="text"  name="user" value="" placeholder="你的mysql用户名" />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">密码</label>
    <div class="controls">
      <input type="text"  name="password" value=""  placeholder="你的mysql用户密码"  />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">数据库</label>
    <div class="controls">
      <input type="text"  name="default_db" value=""  placeholder="你的mysql数据库"  />
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">是否安装演示文件</label>
    <div class="controls">
      <label class="radio inline"><input type="radio" name="setdemo" value="0" checked > 否 </label>
      <label class="radio inline"><input type="radio" name="setdemo" value="1"  > 是 </label>
    </div>
  </div>
  
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">现在安装</button>
  </div>
</form>
