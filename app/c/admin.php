  <?php
class admin extends base{
  function __construct(){
    parent::__construct();
    if(!$this->u['id']){
      redirect('?/login/?&rtu=?/admin/',"请先登录");
    }
    if($this->u['level'] < 10 ) redirect(BASE,'权限不够');
    $this->m = load('m/node_m');

    $sid = $_COOKIE['sid'];
    if(!$sid && seg(2)!='site'){
      redirect('?/admin/site',$param);
    }
    define('SID',$sid);
  }

  function index()
  {
    redirect("/?/admin/page/");
  }

  function site($upid = null ,$uuid = 0)
  {
    switch($upid){
      case 'add':
        $this->site_add($uuid);
        break;
      default:
        $this->site_list($uuid);
    }
  }

  function site_list($uuid = 0)
  {
    if($uuid){
      setcookie('sid',$uuid);
      redirect('?/admin/',$param);
    }
    $param['sites'] = load('m/site_m')->get();
    $this->display('v/admin/site_list',$param);
  }

  function site_add($uuid = 0)
  {
    if($_POST['name']){
      load('m/site_m')->add(array('uuid'=>uniqid(),'name'=>$_POST['name'],'domain'=>$_POST['domain']));
      redirect("?/admin/site");
    }
    $this->display("v/admin/site_add",$param);
  }

  function upload()
  {
    $php_path = dirname(__FILE__) . '/';
    $php_url =  '/';

    //文件保存目录URL
    $save_url = $php_url . 'upload/';
    //定义允许上传的文件扩展名
    $ext_arr = array(
      'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
      'flash' => array('swf', 'flv'),
      'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb'),
      'file' => array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'htm', 'html', 'txt', 'zip', 'rar', 'gz', 'bz2'),
    );
    //最大文件大小
    $max_size = 1000000;

    $save_path ='upload/';


    //PHP上传失败
    if (!empty($_FILES['imgFile']['error'])) {
      switch($_FILES['imgFile']['error']){
        case '1':
          $error = '超过php.ini允许的大小。';
          break;
        case '2':
          $error = '超过表单允许的大小。';
          break;
        case '3':
          $error = '图片只有部分被上传。';
          break;
        case '4':
          $error = '请选择图片。';
          break;
        case '6':
          $error = '找不到临时目录。';
          break;
        case '7':
          $error = '写文件到硬盘出错。';
          break;
        case '8':
          $error = 'File upload stopped by extension。';
          break;
        case '999':
        default:
          $error = '未知错误。';
      }
      $this->json_alert($error);
    }

    //有上传文件时
    if (empty($_FILES) === false) {
      //原文件名
      $file_name = $_FILES['imgFile']['name'];
      //服务器上临时文件名
      $tmp_name = $_FILES['imgFile']['tmp_name'];
      //文件大小
      $file_size = $_FILES['imgFile']['size'];
      //检查文件名
      if (!$file_name) {
        $this->json_alert("请选择文件。");
      }
      //检查目录
      if (@is_dir($save_path) === false) {
        $this->json_alert("上传目录不存在。");
      }
      //检查目录写权限
      if (@is_writable($save_path) === false) {
        $this->json_alert("上传目录没有写权限。");
      }
      //检查是否已上传
      if (@is_uploaded_file($tmp_name) === false) {
        $this->json_alert("上传失败。");
      }
      //检查文件大小
      if ($file_size > $max_size) {
        $this->json_alert("上传文件大小超过限制。");
      }
      //检查目录名
      $dir_name = empty($_GET['dir']) ? 'image' : trim($_GET['dir']);
      if (empty($ext_arr[$dir_name])) {
        $this->json_alert("目录名不正确。");
      }
      //获得文件扩展名
      $temp_arr = explode(".", $file_name);
      $file_ext = array_pop($temp_arr);
      $file_ext = trim($file_ext);
      $file_ext = strtolower($file_ext);
      //检查扩展名
      if (in_array($file_ext, $ext_arr[$dir_name]) === false) {
        $this->json_alert("上传文件扩展名是不允许的扩展名。\n只允许" . implode(",", $ext_arr[$dir_name]) . "格式。");
      }
      //创建文件夹
      if ($dir_name !== '') {
        $save_path .= $dir_name . "/";
        $save_url .= $dir_name . "/";
        if (!file_exists($save_path)) {
          mkdir($save_path);
        }
      }
      $ymd = date("Ymd");
      $save_path .= $ymd . "/";
      $save_url .= $ymd . "/";
      if (!file_exists($save_path)) {
        mkdir($save_path);
      }
      //新文件名
      $new_file_name = date("YmdHis") . '_' . rand(10000, 99999) . '.' . $file_ext;
      //移动文件
      $file_path = $save_path . $new_file_name;
      if (move_uploaded_file($tmp_name, $file_path) === false) {
        $this->json_alert("上传文件失败。");
      }
      @chmod($file_path, 0644);
      $file_url = $save_url . $new_file_name;

      header('Content-type: text/html; charset=UTF-8');
      echo json_encode(array('error' => 0, 'url' => $file_url));
      exit;
    }
  }

  function upload_con($file)
  {
    $php_path = dirname(__FILE__) . '/';
    $php_url =  '/';

    //文件保存目录URL
    $save_url = $php_url . 'upload/';
    //定义允许上传的文件扩展名
    $ext_arr = array(
      'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
      'flash' => array('swf', 'flv'),
      'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb'),
      'file' => array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'htm', 'html', 'txt', 'zip', 'rar', 'gz', 'bz2'),
    );
    //最大文件大小
    $max_size = 1000000;

    $save_path ='upload/';

    //PHP上传失败
    if (!empty($file['error'])) {
      switch($file['error']){
        case '1':
          $error = '超过php.ini允许的大小。';
          break;
        case '2':
          $error = '超过表单允许的大小。';
          break;
        case '3':
          $error = '图片只有部分被上传。';
          break;
        case '4':
          $error = '请选择图片。';
          break;
        case '6':
          $error = '找不到临时目录。';
          break;
        case '7':
          $error = '写文件到硬盘出错。';
          break;
        case '8':
          $error = 'File upload stopped by extension。';
          break;
        case '999':
        default:
          $error = '未知错误。';
      }
      $this->json_alert($error);
    }

    //有上传文件时
    if (empty($file) === false) {
      //原文件名
      $file_name = $file['name'];
      //服务器上临时文件名
      $tmp_name = $file['tmp_name'];
      //文件大小
      $file_size = $file['size'];
      //检查文件名
      if (!$file_name) {
        $this->json_alert("请选择文件。");
      }
      //检查目录
      if (@is_dir($save_path) === false) {
        $this->json_alert("上传目录不存在。");
      }
      //检查目录写权限
      if (@is_writable($save_path) === false) {
        $this->json_alert("上传目录没有写权限。");
      }
      //检查是否已上传
      if (@is_uploaded_file($tmp_name) === false) {
        $this->json_alert("上传失败。");
      }
      //检查文件大小
      if ($file_size > $max_size) {
        $this->json_alert("上传文件大小超过限制。");
      }
      //检查目录名
      $dir_name = empty($_GET['dir']) ? 'image' : trim($_GET['dir']);
      if (empty($ext_arr[$dir_name])) {
        $this->json_alert("目录名不正确。");
      }
      //获得文件扩展名
      $temp_arr = explode(".", $file_name);
      $file_ext = array_pop($temp_arr);
      $file_ext = trim($file_ext);
      $file_ext = strtolower($file_ext);
      //检查扩展名
      if (in_array($file_ext, $ext_arr[$dir_name]) === false) {
        $this->json_alert("上传文件扩展名是不允许的扩展名。\n只允许" . implode(",", $ext_arr[$dir_name]) . "格式。");
      }
      //创建文件夹
      if ($dir_name !== '') {
        $save_path .= $dir_name . "/";
        $save_url .= $dir_name . "/";
        if (!file_exists($save_path)) {
          mkdir($save_path);
        }
      }
      $ymd = date("Ymd");
      $save_path .= $ymd . "/";
      $save_url .= $ymd . "/";
      if (!file_exists($save_path)) {
        mkdir($save_path);
      }
      //新文件名
      $new_file_name = date("YmdHis") . '_' . rand(10000, 99999) . '.' . $file_ext;
      //移动文件
      $file_path = $save_path . $new_file_name;
      if (move_uploaded_file($tmp_name, $file_path) === false) {
        $this->json_alert("上传文件失败。");
      }
      @chmod($file_path, 0644);
      $file_url = $save_url . $new_file_name;

      header('Content-type: text/html; charset=UTF-8');
      $a = array("name"=>$new_file_name,"url"=>$file_url);
      return $a;
    }
  }

  function json_alert($msg) {
    header('Content-type: text/html; charset=UTF-8');
    echo json_encode(array('error' => 1, 'message' => $msg));
    exit;
  }



  function page($upid = '0' ,$id = 0,$model_id = 0)
  {
    switch($upid){
      case 'edit':
        $this->edit($id,$model_id);
        break;
      case 'add':

        $this->add($id,$model_id);
        break;
      case 'del':
        $this->del($id);
        break;
      default:
        $this->plist($upid);
    }
  }

  function plist( $upid = 0 )
  {
    if(isset($_POST['order_by'])){
      foreach($_POST['order_by'] as $k => $v){
        $this->m->update($k, array('order_by'=> $v ));
      }
    }
    $param['uper'] = array();
    $param['upid'] = $upid;

    //上级页面id
    if(!empty($upid)&&$upid!=0){
      $uper_id = $this->m->get($upid);
      $param['uper'][$param['upid']] = $uper_id['title'];
      while ($uper_id['upid']!=0) {
        $a = $uper_id['upid'];
        $uper_id = $this->m->get($uper_id['upid']);
        $param['uper'][$a] = $uper_id['title'];
      }
      $param['uper'] = array_reverse($param['uper'],true);
    }  

    $tot = $this->m->count( " and `type` = 'page' and upid = '$upid' " );
    $psize = 30;
    $pcurrent = isset( $_GET['p'] )? $_GET['p']:0;
    $param['pagination'] = pagination($tot , $pcurrent , $psize ,'/admin/page/index/'.$rel_id.'/?p=');
    $param['records'] = array();
    $a = load('m/node_m')->get( " and `type` = 'page' and sid='".SID."' and upid = '$upid' order by `order`,`id` asc" , $pcurrent ,  $psize);
    foreach ($a as $k => $v) {
      // print_r($v);
      $v['ext1'] = _decode($v['ext1']);
      array_push($param['records'], $v);
    }
    // die;

    // print_r($param);die;
    $this->display('v/admin/index',$param);
  }

  function order_edit($id, $type = '')
  {
    $pnode = $this->m->get($id);
    if($type == 'down') {
      $qnode = $this->m->db->query("select id,`order` from node where `order`>$pnode[order] and `type` = 'page' and upid=$pnode[upid] order by `order` asc limit 1");
      $this->m->update($id, array('order' => $qnode[0]['order']));
      $this->m->update($qnode[0]['id'], array('order' => $pnode['order']));
    } else if($type == 'up') {
      $qnode = $this->m->db->query("select id,`order` from node where `order`<$pnode[order] and `type` = 'page' and upid=$pnode[upid] order by `order` desc limit 1");
      $this->m->update($id, array('order' => $qnode[0]['order']));
      $this->m->update($qnode[0]['id'], array('order' => $pnode['order']));
    }
  }

  function add($upid = 0,$model_id = 0)
  {
    $sid = SID;
    //获取风格、排版、模型列表
    $param['layouts'] = $this->m->db->query("select id,name from node where type = 'layout' and sid = '$sid' order by id");
    $param['templates'] = $this->m->db->query("select id,name from node where type = 'template' and sid = '$sid' order by id");
    $param['models'] = $this->m->db->query("select id,name from node where type = 'model' and sid = '$sid' order by id");
    
    //插入
    if($_POST['name']){
      $_POST['update_date'] = $_POST['create_date'] = time();
      // 默认类型
      if(empty($_POST['ext1']['type'])){
        $_POST['ext1']['type'] = "form";
      }
      if(isset($_FILES)&&!empty($_FILES)){
        foreach ($_FILES as $key => $val){
          $_POST['ext1'][$key]=$this->upload_con($val);
        }
      }
      $_POST['ext1'] = _encode($_POST['ext1']);
      $_POST['sid'] = SID;

      $this->m->add();
      redirect("?/admin/page/$upid/");
    }
    $param['exts'] = array(
        'meta_keywords'=>'meta_keywords',
        'meta_description'=>'meta_description',
        'page_title'=>'page_title',
    );
    // $param['page']['upid'] = $upid;
    if(!empty($upid)&&$upid!=0){
      $uper_id = $this->m->get($upid);
      $param['uper_id'][$upid] = $uper_id['title'];
      while ($uper_id['upid']!=0) {
        $a = $uper_id['upid'];
        $uper_id = $this->m->get($uper_id['upid']);
        $param['uper_id'][$a] = $uper_id['title'];
      }
      $param['uper_id'] = array_reverse($param['uper_id'],true);
    }  

    // 跳转参数
    $param['mod']['upid'] = $upid;
    $param['mod']['url'] = "add";
    // 若没有设置类型，默认读上一级
    if($uuid!="0"){
      $a = $this->m->get($param['page']['upid']);
      $a['ext1'] = _decode($a['ext1']);
      $param['ext1']['template'] = $a['ext1']['son_template'];
      $param['ext1']['layout'] = $a['ext1']['son_layout'];
      if($model_id=="0"){
        $model_id = $a['ext1']['son_model'];
      }
    }
    
    // 获取原型内容列表
    if(!empty($model_id)&&$model_id!=""){
      $param['model_con'] = $this->m->db->query("select content from node where type = 'model' and sid = '$sid' and id = '$model_id' ");
      $param['model_con'] = _decode($param['model_con']['0']['content']);
    }
    //获取原型id
    if(!empty($model_id)&&$model_id!=""){
      $param['ext1']['model'] = $model_id;
    }
    
    // var_dump($param);die;
    $this->display('v/admin/add',$param);
  }

  function del($id)
  {
    $this->m->del($id);
    echo '1';
    exit;
  }

  function get_layout_list()
  {
    $layouts = glob(APP.'v/layout/*.php');
    foreach($layouts as $r){
      $ret[] = str_replace('.php','',basename($r));
    }
    return $ret;
  }

  function edit($id = 0,$model_id = 0)
  {
    $sid = SID;
    //获取风格、排版、模型列表
    $param['layouts'] = $this->m->db->query("select id,name from node where type = 'layout' and sid = '$sid' order by id");
    $param['templates'] = $this->m->db->query("select id,name from node where type = 'template' and sid = '$sid' order by id");
    $param['models'] = $this->m->db->query("select id,name from node where type = 'model' and sid = '$sid' order by id");
    // 修改
    if($_POST['name']){
      $_POST['update_date'] = time();
      // 默认类型
      if(empty($_POST['ext1']['type'])){
        $_POST['ext1']['type'] = "form";
      }
      // var_dump($_FILES);die;
      if(isset($_FILES)&&!empty($_FILES)){
        foreach ($_FILES as $key => $val){
          $pathinfo = pathinfo($val['name']);
          if($pathinfo['extension']){
            $_POST['ext1'][$key]=$this->upload_con($val);
          }
        }
      }
      $_POST['ext1'] = _encode($_POST['ext1']);
      $aa = $this->m->update($id);
      redirect("?/admin/page/".$_POST['upid'].'/');
    }

    $param['page'] = $page = $this->m->get($id);
    // $param['upid'] = $page['upid'];
    if(!empty($page['upid'])&&$page['upid']!=0){
      $uper_id = $this->m->get($page['upid']);
      $param['uper_id'][$page['upid']] = $uper_id['title'];
      while ($uper_id['upid']!=0) {
        $a = $uper_id['upid'];
        $uper_id = $this->m->get($uper_id['upid']);
        $param['uper_id'][$a] = $uper_id['title'];
      }
      $param['uper_id'] = array_reverse($param['uper_id'],true);
    } 
    // 跳转参数
    $param['mod']['url'] = "edit";
    $param['mod']['upid'] = $id;
    $param['uper'] = $this->m->get($page['upid']);
    $param['ext1'] = _decode($page['ext1']);
    $param['exts'] = array(
        'meta_keywords'=>'meta_keywords',
        'meta_description'=>'meta_description',
        'page_title'=>'page_title',
      );
    // 若没有设置类型，默认读上一级
    if(!empty($param['upid'])&&$param['upid']!="0"){
      $mon['ext1'] = _decode($param['uper']['ext1']);
      if(empty($param['ext1']['template'])){
        $param['ext1']['template'] = $mon['ext1']['son_template'];
      }
      if(empty($param['ext1']['layout'])){
        $param['ext1']['layout'] = $mon['ext1']['son_layout'];
      }
      if((empty($model_id)||$model_id==0)&&empty($param['ext1']['model'])){
          $param['ext1']['model'] = $mon['ext1']['son_model'];
      }
    }
    // 修改模型
    if(!empty($model_id)||$model_id!=0){
      $param['ext1']['model'] = $model_id;
    }
    // 获取模型内容列表
    if(!empty($param['ext1']['model'])){
      $id = $param['ext1']['model'];
      $param['model_con'] = $this->m->db->query("select content from node where type = 'model' and sid = '$sid' and id = '$id' ");
      $param['model_con'] = _decode($param['model_con']['0']['content']);
    }
    // 获取模型id
    if(!empty($model_id)){
      $param['ext1']['model'] = $model_id;
    } 
    // var_dump($param);die;
    $this->display('v/admin/add',$param);
  }

  function link($upid = 'list' ,$id = 0 )
  {
    switch($upid){
      case 'edit':
        $this->linkedit($id);
        break;
      case 'add':
        $this->linkadd($id);
        break;
      case 'del':
        $this->linkdel($id);
        break;
      default:
        $this->linklist($upid);
    }
  }

  function linklist($upid)
  {
    if(isset($_POST['order_by'])){
      foreach($_POST['order_by'] as $k => $v){
        $this->m->update($k, array('order_by'=> $v ));
      }
    }
    $param['rel_id'] = $rel_id;
    $tot = $this->m->count( " and `type` = 'link' " );
    $psize = 30;
    $pcurrent = isset( $_GET['p'] )? $_GET['p']:0;
    $param['pagination'] = pagination($tot , $pcurrent , $psize ,'/admin/link/index/'.$rel_id.'/?p=');
    $sid = SID;
    $param['records'] = load('m/node_m')->get( " and `type` = 'link' and `sid` = '$sid' " , $pcurrent ,  $psize);

    $this->display('v/admin/link',$param);
  }

  function linkadd()
  {
    if($_POST['name']){
      $_POST['update_date'] = $_POST['create_date'] = time();
      foreach($_POST['info'] as $info){
        if($info['title'])$ext[] = $info;
      }
      $_POST['ext'] = _encode($ext);
      $_POST['sid'] = SID;
      $this->m->add();
      redirect("?/admin/link/");
    }

    $this->display('v/admin/linkadd',$param);
  }

  function linkdel($id)
  {
    $this->m->del($id);
    echo '1';
    exit;
  }

  function linkedit( $id = 0)
  {
    if($_POST['name']){
      $_POST['update_date'] = time();
      foreach($_POST['info'] as $info){
        if($info['title'])$ext[] = $info;
      }
      $_POST['ext'] = _encode($ext);
      $_POST['sid'] = SID;
      //echo $_POST['ext'];
      $this->m->update($id);
      redirect("?/admin/link/");
    }

    $param['page'] = $page = $this->m->get($id);
    $param['ext'] = _decode($page['ext']);
    $this->display('v/admin/linkadd',$param);
  }

  function usr()
  {
    $records = glob(APP.'v/layout/*.php');
    foreach($records as $r){
      $nr['size'] = filesize($r);
      $nr['name'] = basename($r);
      $nr['time'] = filemtime($r);
     $nnr[] = $nr;
    }
    $this->display('v/admin/layout',array('records'=>$nnr));
  }
  
  function template( $upid = null , $id = 0 ){
    switch($upid){
      case 'edit':
        $this->template_edit($id);
        break;
      case 'add':
        $this->template_add($id);
        break;
      case 'del':
        $this->del($id);
        break;
      default:
        $this->template_list($upid);
    }
  }

  
  function layout( $upid = null , $id = 0 ){
    switch($upid){
      case 'edit':
        $this->template_edit($id,'layout');
        break;
      case 'add':
        $this->template_add($id,'layout');
        break;
      case 'del':
        $this->del($id);
        break;
      default:
        $this->template_list($upid,$type='layout');
    }
  }

  function model( $upid = null , $id = 0 ){
    switch($upid){
      case 'edit':
        $this->template_edit($id,'model');
        break;
      case 'add':
        $this->template_add($id,'model');
        break;
      case 'del':
        $this->del($id);
        break;
      default:
        $this->template_list($upid,$type='model');
    }
  }

  function template_list( $upid = 0 ,$type="template" )
  {
    if(isset($_POST['order_by'])){
      foreach($_POST['order_by'] as $k => $v){
        $this->m->update($k, array('order_by'=> $v ));
      }
    }
    $param['upid'] = $upid;
    $param['uper'] = $this->m->get($upid);
    $param['type'] = $type;
    $psize = 300;
    $pcurrent = isset( $_GET['p'] )? $_GET['p']:0;
    $param['pagination'] = pagination($tot , $pcurrent , $psize ,'/admin/'.$type.'/index/'.$rel_id.'/?p=');
    $param['records'] = load('m/node_m')->get( " and sid='".SID."' and `type` = '".$type."' and upid = '$upid' order by `order`,`id` asc" , $pcurrent ,  $psize);
    $this->display('v/admin/template_list',$param);
  }


  function template_add( $upid = 0 ,$type = 'template')
  {
    load("lib/spyc");
    if($_POST['name']){
      $_POST['update_date'] = $_POST['create_date'] = time();
      $_POST['ext1'] = _encode($_POST['ext1']);
      if($_POST['type']=="model"){
        $_POST['content'] = yaml_parse($_POST['content']);
      }
      // var_dump($_POST);die;
      $id = $this->m->add();
      $layout_file = APP.'v/temp/'.$id.'.php';
      $content = htmlspecialchars_decode($_POST['content']);
      file_put_contents($layout_file, $content); 
      redirect("?/admin/$type/$upid/");
    }
    $param['writable'] = is_writable(APP.'v/temp');
    $param['page']['upid'] = $upid;
    $param['page']['type'] = $type;
    $param['page']['sid'] = $_COOKIE['sid'];
    if($param['page']['type']=="model"){
      $param['page']['content'] = "---
title:
  name: 标题
  type: text
content:
  name: 文章
  type: extension
new:
  name: 新闻
  type: extension
file:
  name: 文件
  type: file
file2:
  name: 图片
  type: file
默认参数，请把这行删除！
";
    }
    $this->display('v/admin/template_add',$param);
  }



  function template_edit( $id = 0,$type = 'template')
  {
    load("lib/spyc");
    if($_POST['name']){
      $_POST['update_date'] = time();
      $_POST['ext1'] = _encode($_POST['ext1']);
      if($_POST['type']=="model"){
        
        $_POST['content'] = yaml_parse($_POST['content']);
      }else{
        $_POST['content'] = htmlspecialchars_decode($_POST['content']);
      }
      unset($_POST['type']);
      $this->m->update($id);
      $layout_file = APP.'v/temp/'.$id.'.php';
      $content = htmlspecialchars_decode($_POST['content']);
      file_put_contents($layout_file, $content); 
      redirect("?/admin/$type/".$_POST['upid'].'/');
    }
    $param['page'] = $page = $this->m->get( $id );
    $param['upid'] = $page['upid'];
    $param['ext1'] = _decode($page['ext1']);
    $param['exts'] = array(
        'meta_keywords'=>'meta_keywords',
        'meta_description'=>'meta_description',
        'page_title'=>'page_title',
      );
    if($page['type'] == "model"){
      $param['page']['content'] = _decode($param['page']['content']);
      $param['page']['content'] = yaml_emit($param['page']['content']);
    }
    // var_dump($param);die;
    $this->display('v/admin/template_add',$param);
  }

  function user()
  {
    $user=$this->m->userlist();
    $param['user']=$user;
    $param['page_title'] = $param['meta_keywords'] = $param['meta_description'] = '用户列表';
    $this->display('v/user/userlist',$param);
  }

  function userdelete(){
    $id=$_GET['id'];
    $this->m->userdelete($id);
    $user=$this->m->userlist();
    $param['user']=$user;
    $this->display('v/user/userlist',$param);
  }

  function update(){

    $conf = array('email'=>'required|email','username'=>'required|isexist','password'=>'required');
    $err = validate($conf);
    $user['id']=$_GET['id'];
    $param['user']=$user;
    if($_POST['password']!=$_POST['repassword']){
      $err['password']='两次密码不一样';
      $param['val'] = $_POST;
      $param['err'] = $err;
      $param['page_title'] = $param['meta_keywords'] = $param['meta_description'] = '更新';
      $this->display('v/user/update',$param);
    }elseif( $err === TRUE) {
      $this->m->userupdate($_POST['id'],$_POST);
      redirect(BASE,'修改成功，请登录。');
    }else {
      $param['val'] = $_POST;
      $param['err'] = $err;
      $param['page_title'] = $param['meta_keywords'] = $param['meta_description'] = '更新';
      $this->display('v/user/update',$param);
    }
  }


  function redirect($info = '')
  {
    $info = urldecode($info);
    redirect($_SERVER['HTTP_REFERER'], $info);
  }

  function tag()
  {
    if($_POST['info']){
      foreach($_POST['info'] as $info){
        if($info['name'])$ext[] = $info;
      }
      $_POST['ext'] = _encode($ext);
      $tags = $this->m->n('tags');
      if(!empty($tags)) {
        $this->m->update($tags['id']);
      } else {
        $_POST['name'] = 'tags';
        $_POST['type'] = 'tag';
        $_POST['create_date'] = $_POST['update_date'] = time();
        $this->m->add();
      }
      redirect("?/admin/tag/");
    }

    $tags = $this->m->n('tags');
    $param['ext'] = _decode($tags['ext']);

    $this->display('v/admin/tag',$param);
  }

  function display($view='v/index',$param = array()){
    $param['menu'] =  array(
      'page'=>'页面',
      'link'=>'链接',
      'tag'=>'标签',
      'template'=>'模板',
      'layout'=>'排板',
      'model'=>'模型',
      'link'=>'链接',
      'tag'=>'标签',
      // 'usr'=>'用户',
      'site'=>'空间',
      'logout'=>'退出',
    );
    $param['al_content'] = view($view,$param,TRUE);
    header("Content-type: text/html; charset=utf-8");
    view('v/admin/template',$param);
  }

  function delimage($id,$model,$sid){
    $con = load("m/node_m")->get(" and `id`= '$id' and `sid` = '$sid'");
    $con['0']['ext1'] = _decode($con['0']['ext1']);
    $con['0']['ext1'][$model] = "";
    $con['0']['ext1'] = _encode($con['0']['ext1']);
    load("m/node_m")->update($id,$con['0']);
  }

  function change_order(){
    
    $id = $_POST['id'];
    $elem['order'] = $_POST['order'];
    $this->m->update($id,$elem);
  }
}

