<?php
load('lib/utility',false);
class base extends c{
  function __construct(){
    global $db_config;
    $this->u = $this->check();
  }
  
  function check()
  {
    $u = load('m/user_m')->check(); 
    return $u;
  }
  
  function display($view='v/index',$param = array())
  {
    $param['al_content'] = view($view,$param,TRUE);
    header("Content-type: text/html; charset=utf-8");
    view('v/template',$param);
  }
}
